// Generates a mesh for the Helmholtz with PML problem. The computational domain
// is [-0.5, 0.5]², and the PML region is 8 cells thick.

comp_corner = 0.5;
l_mesh = 0.02;
corner = comp_corner + 8 * l_mesh;

Point(1) = {-corner, -corner, 0, l_mesh};
Point(2) = {corner, -corner, 0, l_mesh};
Point(3) = {corner, corner, 0, l_mesh};
Point(4) = {-corner, corner, 0, l_mesh};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

Line Loop(5) = {1:4};
Plane Surface(1) = {5};

Physical Surface(1) = {1};
Physical Line(1) = {1:4};

Transfinite Surface{1};
Mesh.RecombineAll = 1;

