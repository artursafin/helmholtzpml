% Compares the FEM solution to an analytic solution.
%   re_paraview - recompute the numerical solution by running python script
%                 which relies on the Paraview package to extract a solution
%                 slice.

function Compare(re_paraview)

if (re_paraview == 1)
	system('python pythonCode.py')
end

A=importdata('solution_slice.csv', ',', 1);
Pabs = sqrt(A.data(:, 1).^2 + A.data(:, 2).^2) + eps;
Pphase = atan2(A.data(:, 2), A.data(:, 1));
X = A.data(:, 3);

xMax = X(end-1);
format shortG

[dom, S] = AnalyticSolution(xMax);

subplot(121)
semilogy(X, Pabs, '.-')
hold on
semilogy(dom, abs(S(:, 3)))
legend('FEM Solution', 'Analytic Free-space')

semilogy([1 1], [3e-5 5e-4])
xlim([0 xMax])
ylim([3e-5 5e-4])
title('Amplitude')
xlabel('x (mm)')
ylabel('$|P(x)|$', 'Interpreter', 'latex')

subplot(122)
xlim([0 xMax])
plot(X, Pphase, '.-')
hold on
plot(dom, atan2(imag(S(:,3)),real(S(:,3))))
plot([1 1], get(gca, 'ylim'))

legend('FEM Solution', 'Analytic Free-space')
xlim([0 xMax])
title('Phase')
xlabel('x (mm)')
ylabel('arg(P(x))')
