# Preconditioner for Helmholtz equation with PML

Hello, and welcome to my repository for a custom preconditioner for the (scalar) Helmholtz equation with the Perfectly Matched Layer (PML)! The approach proposed here is particularly suitable for low-frequency Helmholtz problems with large attenuation coefficient in the PML region. The preconditioner is discussed in some detail in our [SISC publication](https://epubs.siam.org/doi/abs/10.1137/17M1145823) on trace gas sensor modeling.

## Requirements

This project requires the [deal.II package](https://www.dealii.org) with MPI, built against complex [PETSc](https://www.mcs.anl.gov/petsc) compiled with MUMPS, and [p4est](http://www.p4est.org) for mesh partitioning. deal.II may also require the [METIS](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview) package. The mesh was generated using the [gmsh](gmsh.info) package (optional, unless you want to modify the mesh).

For post-processing, we use the [Paraview](https://www.paraview.org/) package to create 1D slices, and Matlab/Octave to compare the finite element solution to the analytic free-space solution.

An outline of how to install these packages to run this code can be viewed [here](https://bitbucket.org/artursafin/Complexpetsc_with_deal.ii).

## Usage

To obtain, compile and execute the project, please run

```
* git clone https://artursafin@bitbucket.org/artursafin/HelmholtzPML.git
* cd HelmholtzPML/build
* cmake ..
* make
* ./run_Precond.sh
```

Note that you can modify the shell script to change solver parameters, number of processors as well as other additional options, which are documented in the `Cpp/main_2D.cpp` class.

If you have Paraview installed, you can run `python pythonCode.py` to generate a slice of the solution along the `y=0` line in a '.csv' format. The solutions can be compared in MATLAB using the Compare() function.

## Authors

* Artur Safin (eawag.ch/~safinart)
* Susan Minkoff (www.utdallas.edu/~sminkoff)
* John Zweck (www.utdallas.edu/~zweck)

## Acknowledgments

Brian Brennan, Robert Kirby (Baylor University), Matthew Knepley (Rice University), Anatoliy Kosterev (Yokogawa Corp.), Noemi Petra (University of California, Merced), and the deal.II and PETSc communities.

