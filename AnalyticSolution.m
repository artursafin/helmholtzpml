% Computes the analytical solution of the free-space Helmholtz equation with
% Sommerfeld radiation conditions.

function [dom, solution] = AnalyticSolution(R)

k = (.526 + 5e-3*1i);
sigma = 7.5e-3;
number_of_steps = 200;

S = @(s) exp(-s.^2/(2*sigma^2));

integrand1 = @(s) s.*besselh(0,1,k*s).*S(s);
integrand2 = @(s) s.*besselj(0,k*s).*S(s);
c1_f = @(r) -pi/(2*1i) * quadgk(integrand1, 0, r, ...
                                 'AbsTol', 1e-10, 'RelTol', 1e-13);
c2_f = @(r) pi/(2*1i) * quadgk(integrand2, 0, r, ...
                                 'AbsTol', 1e-10, 'RelTol', 1e-13);

R_inf = 0.5;      % Range at which c1(r) ~ c1(infinity)
c1 = c1_f(R_inf);
c2 = c2_f(R_inf);

u = @(r) (c1_f(r)-c1) * besselj(0, k*r) + c2_f(r) * besselh(0,1,k*r);
u_inf = @(r) c2 * besselh(0,1,k*r);

% Finally we can compute the solution
solution = zeros(number_of_steps+1, 3);
for n = 0:number_of_steps
    if(n == 0)
        temp = -c1 * besselj(0, 0);
    else
        dist = (n/number_of_steps) * R;
        if (dist < R_inf)
            temp = u(dist);
        else
            temp = u_inf(dist);
        end
    end
    
    solution(n+1,1) = abs(temp);
    solution(n+1,2) = atan2(imag(temp), real(temp));
    solution(n+1,3) = temp;
end

dom = 0:(R/number_of_steps):R;
