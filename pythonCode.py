# Extracts a solution slice along the y=0 line. The output format can be read
# by matlab.
from paraview.simple import *
import sys

reader = OpenDataFile("output/w_solution.pvtu")

slice_filter = Slice(reader)
slice_filter.SliceType.Normal = [0.0, 1.0, 0.0]
slice_filter.SliceOffsetValues = [0.0]
slice_filter.SliceType = "Plane"

writer = CreateWriter("solution_slice.csv", slice_filter)
writer.UpdatePipeline()

import numpy
import csv
csvData = numpy.loadtxt("solution_slice.csv", delimiter=",", skiprows=1, dtype=None)
csvData=csvData[csvData[:, 2].argsort()]
with open("solution_slice.csv", 'wb') as f:
	f.write(b'"P1","P2","X","Y","Z"\n')
	numpy.savetxt(f, csvData, delimiter=",", fmt="%g")

