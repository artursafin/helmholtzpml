mpirun -np 8 --oversubscribe ./main_2D \
   -kScale 1 \
   -nPreRefinements 2 \
   -nIter 50 \
\
   -ksp_type fgmres \
   -pc_mg_type full \
   -mg_levels_pc_type sor \
   -mg_levels_pc_sor_omega 1 \
   -mg_levels_ksp_type gmres \
   -mg_levels_ksp_max_it 5 \
\
   -ksp_gmres_restart 100 \
   -ksp_monitor_true_residual \
#   -log_view    # Optional profiling by PETSc
