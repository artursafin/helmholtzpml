// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
HelmholtzDD<dim> :: HelmholtzDD() :
   triangulation(MPI_COMM_WORLD,
                 typename Triangulation<dim>::MeshSmoothing
                 (Triangulation<dim>::smoothing_on_refinement |
                  Triangulation<dim>::limit_level_difference_at_vertices)),
   fe(1),
   dof_handler(triangulation),
   rank(Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)),
   world_size(Utilities::MPI::n_mpi_processes(MPI_COMM_WORLD)),
   pcout(std::cout, rank == 0),
   timer(MPI_COMM_WORLD, pcout,
         TimerOutput::summary, TimerOutput::wall_times),
   k(0.5, 5e-3)
{
   // Can supply a different wavenumber by passing a run-time parameter kScale.
   // The default k is equivalent to about 1/2 when non-dimensionalized.
   PetscReal scale_k = 1;
   CHKERRXX(PetscOptionsGetReal(NULL, NULL, "-kScale", &scale_k, NULL));

   // Scale k and precompute k^2
   k = k * scale_k;
   ks = k*k;
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void HelmholtzDD<dim> :: create_grid()
{
   GridIn<dim> import_mesh;
   import_mesh.attach_triangulation(triangulation);
   std::ifstream mesh_input ("../mesh.msh");
   import_mesh.read_msh(mesh_input);

   // Set material id for comp & PML cells.
   for (const auto &cell: dof_handler.active_cell_iterators())
   {
      const Point<dim> cell_center = cell -> center();
      if ((abs(cell_center(0)) < pml_a) && (abs(cell_center(1)) < pml_a))
         cell -> set_material_id(comp_id);
      else
         cell -> set_material_id(pml_id);
   }

   // Refine in the computational domain + immediately adjacent cells.
   PetscInt n_comp_ref = 0;
   CHKERRXX(PetscOptionsGetInt(NULL, NULL, "-nPreRefinements",
            &n_comp_ref, NULL));

   const double l_mesh = 0.02;
   const double refinement_thickness = 1;
   for (PetscInt count = 0; count < n_comp_ref; ++count)
   {
      for (const auto &cell: dof_handler.active_cell_iterators())
      {
         const Point<dim> cell_center = cell -> center();
         if ((abs(cell_center(0)) < pml_a + refinement_thickness*l_mesh) &&
             (abs(cell_center(1)) < pml_a + refinement_thickness*l_mesh))
         {
            cell -> set_refine_flag();
         }
      }
      triangulation.execute_coarsening_and_refinement();
   }
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void HelmholtzDD<dim> :: setup_system()
{
   // Allocate and determine the index sets of locally owned and relevant DoFs.
   dof_handler.distribute_dofs(fe);
   pcout << "# of complex DoFs: " << dof_handler.n_dofs() << endl;

   locally_owned_dofs = dof_handler.locally_owned_dofs();
   DoFTools :: extract_locally_relevant_dofs(dof_handler,
                                             locally_relevant_dofs);

   // Determine domain association for each DoF.
   index_sets();

   // Vector initialization.
   u.reinit(locally_owned_dofs, locally_relevant_dofs, MPI_COMM_WORLD);
   b.reinit(locally_owned_dofs, MPI_COMM_WORLD);

   DynamicSparsityPattern dsp, dsp_comp;
   // Sparsity pattern, matrix initialization.
   initialize_sparsity(hanging_nodes, dsp);
   A.reinit(locally_owned_dofs, locally_owned_dofs,
            dsp, MPI_COMM_WORLD);
   MassM.reinit(locally_owned_dofs, locally_owned_dofs,
                dsp, MPI_COMM_WORLD);
   
   // Same, but only for matrices strictly inside the computational domain.
   initialize_sparsity(hanging_nodes_comp, dsp_comp);
   P_comp.reinit(locally_owned_dofs, locally_owned_dofs,
                 dsp_comp, MPI_COMM_WORLD);
   Mass_comp.reinit(locally_owned_dofs, locally_owned_dofs,
                    dsp_comp, MPI_COMM_WORLD);
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void HelmholtzDD<dim> :: index_sets()
{
   // We generate the index sets for the unknowns in each subdomain. Steps:
   // 1) Create a distributed vector for each domain that is the size of the
   //    solution vector on the entire domain.
   PETScWrappers::MPI::Vector comp_dofs;
   comp_dofs.reinit(locally_owned_dofs, MPI_COMM_WORLD);
   
   // 2) Cycle through all cells in each domain, and set the value of all the
   //    locally *active* DoFs on the cell equal to 1
   std::vector<types::global_dof_index> local_dof_indices (fe.dofs_per_cell);
   for (const auto &cell: dof_handler.active_cell_iterators())
      if ((cell -> is_locally_owned()) && (cell -> material_id() == comp_id))
      {
         cell -> get_dof_indices(local_dof_indices);
         for (unsigned int i = 0; i < fe.dofs_per_cell; ++i)
            comp_dofs[local_dof_indices[i]] += 1.0;
      }

   // 3) Since locally active DoFs may belong to other processors, we exchange
   //    information between processors by adding the values up for each DoF.
   //    After the exchange, if the vector value is zero for a locally owned
   //    DoF, then the DoF is in the interior of that domain.
   comp_dofs.compress(VectorOperation::add);

   // Notify the constraint matrix of hanging nodes.
   hanging_nodes.clear();
   hanging_nodes.reinit(locally_relevant_dofs);
   DoFTools::make_hanging_node_constraints(dof_handler,
                                           hanging_nodes);
   hanging_nodes_comp.clear();
   hanging_nodes_comp.reinit(locally_relevant_dofs);
   DoFTools::make_hanging_node_constraints(dof_handler,
                                           hanging_nodes_comp);

   // Integers that describe the first and last indices of locally owned dofs.
   local_row_start = locally_owned_dofs.nth_index_in_set(0);
   local_row_end = local_row_start + locally_owned_dofs.n_elements();

   // Set DoFs to zero for the preconditioner in the computational domain. This
   // will also reduce the number of unknowns allocated for the sparsity
   // pattern.
   for (auto i = (types::global_dof_index) local_row_start;
		  i < (types::global_dof_index) local_row_end; ++i)
      if ((PetscScalar) comp_dofs(i) == 0.0)
         hanging_nodes_comp.add_line(i);

   // We now apply boundary conditions. Note that only the PML region has a
   // non-interior boundary.
   DoFTools::make_zero_boundary_constraints(dof_handler, 1, hanging_nodes);
   hanging_nodes.close();
   hanging_nodes_comp.close();

   // Finally, we establish the IndexSets for each locally_owned domain. The
   // results are stored into a std::vector<PetscInt> and later converted into
   // a PETSc IS (IndexSet) object for the solve() step.

   // First, we count the number of dofs stored on each processor.
   unsigned int n_comp_dofs = 0,
                n_pml_dofs = 0;
   comp_dof_indices.resize(locally_owned_dofs.n_elements());
   pml_dof_indices.resize(locally_owned_dofs.n_elements());
   
   // Each processor owns a contiguous chunk of unknowns. The two variables
   // determine where this chunk begins and ends.
   local_row_start = locally_owned_dofs.nth_index_in_set(0);
   local_row_end = local_row_start + locally_owned_dofs.n_elements();
   
   for (auto i = (types::global_dof_index) local_row_start;
      i < (types::global_dof_index) local_row_end; ++i)
   {
      if ((PetscScalar) comp_dofs(i) != 0.0)
      {
         comp_dof_indices[n_comp_dofs] = i;
         ++n_comp_dofs;
      }
      else
      {
         pml_dof_indices[n_pml_dofs] = i;
         ++n_pml_dofs;
      }
   }

   // Eliminate unnecessary zeros at the end of the arrays.
   comp_dof_indices.resize(n_comp_dofs);
   pml_dof_indices.resize(n_pml_dofs);
   
   // Output stats for each subdomain
   int global_pml_count, local_pml_count = n_pml_dofs,
       global_comp_count, local_comp_count = n_comp_dofs;
   MPI_Reduce(&local_pml_count, &global_pml_count, 1,
              MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
   MPI_Reduce(&local_comp_count, &global_comp_count, 1,
              MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

   pcout << "Problem dimensions:" << endl
         << " Domain\t\tDoF count\t     %" << endl
         << "-----------------------------------------------" << endl
         << " PML\t\t" << global_pml_count << "\t\t"
         << (double) global_pml_count / dof_handler.n_dofs() * 100
         << "%" << endl
         << " Comp\t\t" << global_comp_count << "\t\t"
         << (double) global_comp_count / dof_handler.n_dofs() * 100
         << "%" << endl
         << "-----------------------------------------------" << endl;
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void HelmholtzDD<dim> :: assemble_system()
{
   // Quadrature, basis function values for interior assembly
   QGauss<dim> quadrature_formula (2);
   QGauss<dim-1> face_quadrature_formula (2);

   FEValues<dim> fe_values (fe, quadrature_formula,
                            update_values | update_gradients |
                            update_quadrature_points | update_JxW_values);
   FEFaceValues<dim> fe_face_values (fe, face_quadrature_formula,
                                     update_values | update_quadrature_points |
                                     update_JxW_values);

   const unsigned int dofs_per_cell = fe.dofs_per_cell,
                      face_dofs_per_cell = fe_face_values.dofs_per_cell,
                      n_quad = quadrature_formula.size(),
                      n_face_quad = face_quadrature_formula.size(),
                      n_cell_faces = GeometryInfo<dim>::faces_per_cell;

   std::vector<types::global_dof_index> local_dof_indices (dofs_per_cell);

   FullMatrix<PetscScalar> cell_A (dofs_per_cell, dofs_per_cell);
   FullMatrix<PetscScalar> cell_Mass (dofs_per_cell, dofs_per_cell);
   Vector<PetscScalar> cell_b (dofs_per_cell);

   // RHS of the Helmholtz equation of the form f = exp[ -||x||²/(2σ²) ]
   auto RHS = [sigma=sigma] (const Point<dim> &p) -> PetscScalar {
      return exp(-p.square() / (2 * sigma * sigma));
   };

   // Attenuation function of the form β(x) = 1 + i*A(x-a)²/(b-a)². (See any
   // PML paper for the definitions). Here we just compute the imaginary part.
   auto PML_coeff = [this, power = 2] (const double x_i) -> double {
      if (abs(x_i) < pml_a)
         return 0;
      else
         return coeff * pow(abs(x_i) - pml_a, power) / pow(pml_thick, power);
   };

   Tensor<2, dim, PetscScalar> B = Tensor<2, dim, PetscScalar> ();

   for (const auto &cell: dof_handler.active_cell_iterators())
   {
      if (cell -> is_locally_owned())
      {
         fe_values.reinit(cell);
         cell_A = PetscScalar (0.0);
         cell_Mass = PetscScalar (0.0);
         cell_b = PetscScalar (0.0);
         
         for (unsigned int q = 0; q < n_quad; ++q)
         {
            const Point<dim> pt = fe_values.quadrature_point(q);
            const PetscScalar beta_x (1, PML_coeff(pt(0)));
            const PetscScalar beta_y (1, PML_coeff(pt(1)));

            B[0][0] = beta_y / beta_x;
            B[1][1] = beta_x / beta_y;

            for (unsigned int i = 0; i < dofs_per_cell; ++i)
            {
               for (unsigned int j = 0; j < dofs_per_cell; ++j)
               {
                  cell_Mass(i, j) +=
                        fe_values.shape_value(j, q) *
                        fe_values.shape_value(i, q) *
                        fe_values.JxW(q);

                  cell_A(i, j) += (
                     -  B *
                        fe_values.shape_grad(j, q) *
                        fe_values.shape_grad(i, q)
                     +  ks * beta_x * beta_y *
                        fe_values.shape_value(j, q) *
                        fe_values.shape_value(i, q))
                     *
                        fe_values.JxW(q);
               }

               cell_b(i) +=
                        fe_values.shape_value(i, q) *
                        RHS(fe_values.quadrature_point(q)) *
                        fe_values.JxW(q);
            }
         }
         cell -> get_dof_indices(local_dof_indices);
         hanging_nodes.distribute_local_to_global(cell_A, cell_b,
                                                  local_dof_indices,
                                                  A, b);
         hanging_nodes.distribute_local_to_global(cell_Mass,
                                                  local_dof_indices,
                                                  MassM);

         // Assemble the boundary term for the preconditioner.
         if (cell -> material_id() == comp_id)
         {
            for (unsigned int face_id = 0; face_id < n_cell_faces; ++face_id)
            {
               if ((cell -> face(face_id) -> at_boundary() == false) &&
                   (cell -> neighbor(face_id) -> material_id() == pml_id))
               {
                  fe_face_values.reinit(cell, face_id);
                  for (unsigned int q = 0; q < n_face_quad; ++q)
                     for (unsigned int i = 0; i < face_dofs_per_cell; ++i)
                        for (unsigned int j = 0; j < face_dofs_per_cell; ++j)
                        {
                           cell_A(i, j) +=
                                 PETSC_i * k *
                                 fe_face_values.shape_value(j, q) *
                                 fe_face_values.shape_value(i, q) *
                                 fe_face_values.JxW(q);
                        }
               }
            }
            hanging_nodes_comp.distribute_local_to_global(cell_A,
                                 local_dof_indices, P_comp);
            hanging_nodes_comp.distribute_local_to_global(cell_Mass,
                                 local_dof_indices, Mass_comp);
         }
      }
   }

   A.compress(VectorOperation::add);
   P_comp.compress(VectorOperation::add);

   MassM.compress(VectorOperation::add);
   Mass_comp.compress(VectorOperation::add);

   b.compress(VectorOperation::add);
}

// ██████████████████████████████████████████████████████████████████████████ //

template <int dim>
void HelmholtzDD<dim> :: solve()
{
   timer.enter_subsection("Setup Solver");

   // Data structure that contains all the PETSc objects
   PetscData *data = new PetscData;

   // Set up index sets for subdomains
   IS_Convert(comp_dof_indices, data->comp_IS);
   IS_Convert(pml_dof_indices, data->pml_IS);
   
   data->Pcomp = P_comp;
   data->MassComp = Mass_comp;
   data->MassM = MassM;

   // Obtain restricted versions of the submatrices
   CHKERRXX(MatCreateSubMatrix(P_comp, data->comp_IS, data->comp_IS,
                               MAT_INITIAL_MATRIX, &data->Pcomp_r));
   CHKERRXX(MatCreateSubMatrix(Mass_comp, data->comp_IS, data->comp_IS,
                               MAT_INITIAL_MATRIX, &data->MassComp_r));
   CHKERRXX(MatCreateSubMatrix(A, data->pml_IS, data->pml_IS,
                               MAT_INITIAL_MATRIX, &data->Ppml_r));
   CHKERRXX(MatCreateSubMatrix(A, data->pml_IS, data->comp_IS,
                               MAT_INITIAL_MATRIX, &data->A21_r));

   // Initialize solution vector u
   PETScWrappers::MPI::Vector u_distributed;
   u_distributed.reinit(locally_owned_dofs, MPI_COMM_WORLD);
   
   // Initialize auxiliary vectors
   CHKERRXX(VecDuplicate(b, &temp));
   CHKERRXX(VecDuplicate(b, &rhs));

   // Set up global problem
   KSP ksp;
   KSPCreate(MPI_COMM_WORLD, &ksp);
   KSPSetOperators(ksp, A, A);
   KSPSetType(ksp, "gmres");
   
   PetscInt n_iter = 20;
   CHKERRXX(PetscOptionsGetInt(NULL, NULL, "-nIter", &n_iter, NULL));
   KSPSetTolerances(ksp, 1e-6, 1e-15, PETSC_DEFAULT, n_iter);
   CHKERRXX(KSPSetResidualHistory(ksp, NULL, n_iter, PETSC_FALSE));
   CHKERRXX(KSPSetFromOptions(ksp));

   PC pc;
   KSPGetPC(ksp, &pc);
   PCSetType(pc, "shell");
   PCShellSetContext(pc, data);
   PCShellSetApply(pc, pressurePC);
   SetUpPressureBlock(data);
   
   timer.leave_subsection();

   // Solve the system
   timer.enter_subsection("Solve");
   CHKERRXX(KSPSolve(ksp, b, u_distributed));
   pcout << "Solved" << endl;

   hanging_nodes.distribute(u_distributed);
   u = u_distributed;

   const PetscReal *residuals;
   PetscInt n_actual_iter;
   CHKERRXX(KSPGetResidualHistory(ksp, &residuals, &n_actual_iter));
   
   pcout << "\nNumber of iterations: " << n_actual_iter-1 << endl
         << "Residuals:" << endl;
   for (PetscInt i = 0; i < n_actual_iter; ++i)
      pcout << i << "\t" << residuals[i] << endl;
   
   timer.leave_subsection();

   // Destroy PETSc objects
   KSPDestroy(&ksp);
   petsc_destroy(data);
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void HelmholtzDD<dim> :: export_solution()
{
   DataOut<dim> data_out;
   data_out.attach_dof_handler(dof_handler);

   data_out.add_data_vector(u, "u");
   data_out.build_patches();

   std::string filename = "../output/solution_proc_"
                        + Utilities::int_to_string(rank, 2)
                        + ".vtu";
   
   std::ofstream output(filename.c_str());
   data_out.write_vtu(output);
   
   if (rank == 0)
   {
      std::vector<std::string> filenames;
      for (unsigned int i = 0; i < world_size; ++i)
      {
         filenames.push_back("solution_proc_"
                           + Utilities::int_to_string(i, 2)
                           + ".vtu");
      }

      std::string main_filename = "../output/w_solution.pvtu";
      std::ofstream full_output(main_filename);
      data_out.write_pvtu_record(full_output, filenames);
   }
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void HelmholtzDD<dim> :: run()
{
   timer.enter_subsection("Import Grid");
   create_grid();
   timer.leave_subsection();

   timer.enter_subsection("Setup System");
   setup_system();   
   timer.leave_subsection();
      
   timer.enter_subsection("Assemble System");
   assemble_system();
   pcout << "System Assembled" << endl << endl;
   timer.leave_subsection();

   solve();
   
   timer.enter_subsection("Export Solution");
   export_solution();
   timer.leave_subsection();
}
