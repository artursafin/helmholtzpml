// This file consists of functions that are unlikely to be modified.
// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
HelmholtzDD<dim> :: ~HelmholtzDD()
{
   dof_handler.clear();
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void HelmholtzDD<dim> ::
   initialize_sparsity(const AffineConstraints<PetscScalar> &hanging_nodes_arg,
                       DynamicSparsityPattern &dsp_arg) const
{
   // Initializes the sparsity pattern & exchange local row information to
   // find where are the 'ghost' entries
   dsp_arg.reinit(dof_handler.n_dofs(), dof_handler.n_dofs(),
                  locally_relevant_dofs);
   DoFTools::make_sparsity_pattern(dof_handler, dsp_arg,
                                   hanging_nodes_arg, false);
   SparsityTools::distribute_sparsity_pattern(dsp_arg,
                                              locally_owned_dofs,
                                              MPI_COMM_WORLD,
                                              locally_relevant_dofs);
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void HelmholtzDD<dim> :: IS_Convert(const std::vector<PetscInt> &vector_IS,
                                    IS &petsc_IS) const
{
   // Compatibility function - converts an index set in std::vector format to
   // a PETSc IS object.
   PetscInt *dof_indices = new PetscInt[vector_IS.size()];
   for (unsigned int i = 0; i < vector_IS.size(); ++i)
      dof_indices[i] = vector_IS[i];
   
   ISCreateGeneral(MPI_COMM_WORLD, vector_IS.size(),
                   dof_indices, PETSC_COPY_VALUES,
                   &petsc_IS);
}

