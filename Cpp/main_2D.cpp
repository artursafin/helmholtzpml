/* Solves the Helmholtz problem with the perfectly matched layers (PML) boundary
condition using GMRES with a custom preconditioner. We found this approach
to be particularly suitable for solving the Morse-Ingard equations with PML.

Domain: unit (1x1)
Source term: f = exp(-||x||² / (2σ²)), σ=7.5e-3.
Wavenumber: k = 0.5 + 0.5e-3i

This is with Taylor 0-th order transmission conditions. We did not observe much
improvement (or any) with 2-nd order transmission conditions.

 -nPreRefinements: number of pre-refinements to perform in the computational
                   region. Default: 0
 -kScale:          scale the wavenumber by a constant. Default: 1
 -nIter:           maximum number of outer GMRES iterations. Default: 20.

Note: PetscScalar = complex<double>.
*/

#include "HelmholtzDD.h"

int main(int argc, char *argv[])
{
   try
   {
      Utilities::MPI::MPI_InitFinalize mpi_initialization (argc, argv, 1);
      {
         HelmholtzDD<2> pde;
         pde.run();
      }
   }
   catch (std::exception &exc)
   {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Exception on processing: " << std::endl
                << exc.what() << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;
   }
   catch (...)
   {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Unknown exception!" << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;
   }
}
