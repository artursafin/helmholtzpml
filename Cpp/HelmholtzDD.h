#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/conditional_ostream.h>
#include <deal.II/base/utilities.h>
#include <deal.II/base/index_set.h>
#include <deal.II/base/timer.h>

#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/affine_constraints.h>
#include <deal.II/lac/sparsity_tools.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>

#include <deal.II/grid/grid_in.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/fe_q.h>

#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/data_out.h>

#include <deal.II/distributed/tria.h>
#include <deal.II/lac/petsc_sparse_matrix.h>
#include <deal.II/lac/petsc_vector.h>

#include <fstream>

using namespace dealii;

#include "PetscObjects.cpp"

using std::endl;

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
class HelmholtzDD
{
public:
   HelmholtzDD();
   ~HelmholtzDD();
   void run();

private:
   void create_grid();
   void setup_system();
   void index_sets();
   
   void assemble_system();
   void solve();

   void export_solution();

   // Determine sparsity pattern for a matrix or a preconditioner.
   void initialize_sparsity(const AffineConstraints<PetscScalar> &hanging_nodes_arg,
                            DynamicSparsityPattern &dsp_arg) const;

   // Initializes index sets for the subdomains. Note that the parameter
   // vector_IS should already contain the index set for the *complex* vector.
   void IS_Convert(const std::vector<PetscInt> &vector_IS,
                   IS &petsc_IS) const;

   parallel::distributed::Triangulation<dim> triangulation;
   FE_Q<dim> fe;                // FE Space for P
   
   DoFHandler<dim> dof_handler;

   // Variables for the global problem
   AffineConstraints<PetscScalar> hanging_nodes;  // For handling adaptive refinements
   PETScWrappers::MPI::SparseMatrix A, MassM;
   PETScWrappers::MPI::Vector u, b;

   // Matrices, sparsity patterns and constraint operators for each subdomain.
   // Computational domain:
   AffineConstraints<PetscScalar> hanging_nodes_comp;
   PETScWrappers::MPI::SparseMatrix P_comp, Mass_comp;

   // Index sets that contain DoFs owned by a processor, and locally active DoFs
   IndexSet locally_relevant_dofs, locally_owned_dofs;

   // Global indices as to where the local dofs start and end.
   PetscInt local_row_start, local_row_end;
   
   // Current processor rank, number of processors
   const unsigned int rank, world_size;
   
   ConditionalOStream pcout;        // Displays output *only* from rank 0
   TimerOutput timer;               // Wall clock time
   
   // Wavenumber k, and ks = k². sigma is the width of the laser beam.
   PetscScalar k, ks;
   const double sigma = 7.5e-3;

   // PML domain values
   const double pml_a = 0.5, pml_thick = 0.16, coeff = 100;

   // Index sets for the complex unknowns in each subdomain. 
   std::vector<PetscInt> comp_dof_indices, pml_dof_indices;
   
   // Mesh subdomain and boundary markers
   const unsigned int comp_id = 1, pml_id = 2;
};

#include "AuxiliaryFunctions.cpp"
#include "HelmholtzDD.cpp"
